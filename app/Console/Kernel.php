<?php

namespace App\Console;

use App\Models\Post;
use App\Http\Controllers\SocialMediaController;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use SoapVar;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {
            $day = date('N');
            $hour = date('H:i');

            $posts = Post::join('schedules', 'schedules.id', '=', 'posts.date')->select(
                'posts.id',
                'posts.status',
                'posts.social_media_id',
                'posts.title',
                'posts.description',
                'schedules.day',
                'schedules.hour',
            )->where('posts.status', 'Pending')->get();

            foreach ($posts as $post) {
                try {
                    logger($day);
                    logger($hour);
                    logger($post->day);
                    logger($post->hour);

                    if ($post->day == $day . "" && $post->hour == $hour . "") {
                        logger('here');
                        $post->update(["status" => "Ready"]);
                        $controller = new SocialMediaController;
                        logger($controller->publish($post));
                    }
                } catch (\Throwable $th) {
                    logger($th);
                }
            }
        })->everyMinute();
    }







    // $schedule->command('inspire')->hourly();

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
