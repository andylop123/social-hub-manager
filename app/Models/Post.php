<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = ['status'];

    public function socialmedia()
    {
        return $this->belongsTo(SocialMedia::class,'social_media_id');
    }

    public function schedule(){
        return $this->belongsTo(Schedule::class,'date');
    }

}
