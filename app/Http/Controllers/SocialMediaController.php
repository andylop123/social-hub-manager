<?php

namespace App\Http\Controllers;

use App\Models\SocialMedia;
use App\Models\Post;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Abraham\TwitterOAuth\TwitterOAuth;
use Illuminate\Support\Facades\Auth;

class SocialMediaController extends Controller
{
    public function posts()
    {
        $social_media = SocialMedia::all();
        $posts = Post::where('user_id', Auth::user()->id)->Where('state', 'active')->get();
        return view('posts', ['social_media' => $social_media, 'posts' => $posts]);
    }

    //Todo: This method allow the user to post a tweet is just add change to real work
    public function store()
    {
        $atributes = request()->validate([
            'user_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'social_media_id' => 'required'
        ]);
        $social_media = request()->social_media_id;
        foreach ($social_media as $media) {
            $atributes['social_media_id'] = $media;
            $atributes['date'] = request()->date != null ? request()->date : 0;

            $post = Post::create($atributes);
            if (!request()->date != null) {
                $post->update(["status" => "Ready"]);
                $this->publish($post);
            }
        }
        return redirect('/dashboard')->with('success', 'Your record has been created.');
    }

    public function post()
    {
        $social_media = SocialMedia::all();
        $schedules = Schedule::where('user_id', Auth::user()->id)->Where('state', 'active')->get();
        return view('post', ['socials' => $social_media, 'schedules' => $schedules]);
    }


    public function publish($post)
    {
        switch ($post->socialmedia->name) {
            case 'Facebook':
                // $newWord = str_replace(' ', '%20', $post->description);
                // $token = "EAAIwogEy5ZB8BAEbyjHpGip8HIfZCsCAGBPvbZAhLmFFD4nlzrjSOd2JSCG1lewLlrKJdiA7G0w7C6ZB0ZCqvle2BDOpJxTEjJahxnWQMdrrsXvGGZALpZCHqHroKJY6Q0p4ZBGt6LhDLxYCjq7GAfcoMDtBZC78Abk8sV3dbkrDlSYwGmIE4urlM0rxq2jMLn54ZD";

                // shell_exec('curl -i -X POST \ "https://graph.facebook.com/v12.0/101008865759103/feed?message=' . $newWord . '&access_token=' . $token . '"   ');
                // return 'facebook';
                $newWord = str_replace(' ', '%20', $post->description);
                $token = "EAAIwogEy5ZB8BAEbyjHpGip8HIfZCsCAGBPvbZAhLmFFD4nlzrjSOd2JSCG1lewLlrKJdiA7G0w7C6ZB0ZCqvle2BDOpJxTEjJahxnWQMdrrsXvGGZALpZCHqHroKJY6Q0p4ZBGt6LhDLxYCjq7GAfcoMDtBZC78Abk8sV3dbkrDlSYwGmIE4urlM0rxq2jMLn54ZD";
                // shell_exec("sudo su");
                // shell_exec('curl -i -X POST \ "https://graph.facebook.com/v12.0/101008865759103/feed?message=' . $newWord . '&access_token=' . $token . '"   ');
                // return 'facebook';

                $apiURL = 'https://graph.facebook.com/v12.0/101008865759103/feed';
                $postInput = [
                    'message' => $newWord,
                    'access_token' => $token
                ];

                $client = new \GuzzleHttp\Client();
                $response = $client->request('POST', $apiURL, ['form_params' => $postInput]);

                $statusCode = $response->getStatusCode();
                $responseBody = json_decode($response->getBody(), true);

                $post->update(["status" => "Ready"]);
                return "facebook";
            case 'Twitter':
                $connection = new TwitterOAuth(
                    'ivkwRyEvIHzha1xpXQ3SRRhlP', // Information about your twitter API
                    'QOQMDTxkeiIFw3372YlF0cS8JKxp41nzROl4e5xtYNhQWFC8BG', // Information about your twitter API
                    '1464004751209730050-Q0KYVfmYFitCZX7GCt1OAQ4eAu6WG4', // You get token from user, when him  sigin to your app by twitter api
                    'CiYWBw24iLCUxVTcpHfpWFsdqjUqzF763o11KBPkErcLt' // You get tokenSecret from user, when him  sigin to your app by twitter api
                );
                $connection->post("statuses/update", ["status" => $post->description]);
                $post->update(["status" => "Ready"]);
                return 'Twitter';

        }
    }
}
