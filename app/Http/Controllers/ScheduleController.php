<?php

namespace App\Http\Controllers;

use DateTime;
use App\Models\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ScheduleController extends Controller
{

    public function index()
    {
        $schedules = Schedule::where('user_id', Auth::user()->id)->Where('state', 'active')->get();
        return view('schedule', ['schedules' => $schedules]);
    }

    public function post()
    {
        $days = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday'
        ];
        return view('schedulePost', ['days' => $days]);
    }

    public function store()
    {
        $atributes = request()->validate([
            'user_id' => 'required',
            'day' => 'required',
            'hour' => 'required',
        ]);

        Schedule::create($atributes);
        return redirect('/schedule')->with('success', 'Your schedule has been created.');
    }

    public function remove($id)
    {
        $schedule = Schedule::find($id);
        $schedule->update(['state' => 'inactive']);
        return back()->with('success', 'schedole deleted');
    }

    public function showEdit($id)
    {
        $schedule = Schedule::find($id);
        $days = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday'
        ];
        return view('edit', ['days' => $days, 'schedule' => $schedule]);
    }




    public function edit($id)
    {

        $schedule = Schedule::find($id);

        $atributes = request()->validate([
            'user_id' => 'required',
            'day' => 'required',
            'hour' => 'required',
        ]);
        $schedule->update($atributes);



        return redirect('/dashboard')->with('success', 'Your schedule has been updated.');
    }
}
