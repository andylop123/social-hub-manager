<?php

use App\Models\SocialMedia;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SocialMediaController;
use App\Http\Controllers\ScheduleController;
use App\Models\Schedule;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::post('newpost', [SocialMediaController::class, 'store']);


Route::get('/dashboard', [SocialMediaController::class, 'posts'])->middleware(['auth'])->name('dashboard');
Route::get('/post', [SocialMediaController::class, 'post'])->middleware(['auth'])->name('post');
Route::get('showedit/{post:id}', [ScheduleController::class, 'showEdit'])->middleware(['auth'])->name('edit');

Route::get('delete/{post:id}', [ScheduleController::class, 'remove'])->middleware(['auth']);
Route::post('edit/{post:id}', [ScheduleController::class, 'edit'])->middleware(['auth']);

Route::get('login/{provider}',  [SocialMediaController::class, 'index'])->middleware(['auth'])->name('socialMedia');

Route::get('/schedule',[ScheduleController::class,'index']);
Route::post('/schedule',[ScheduleController::class,'store']);
Route::get('schedule/{schedule:id}',[ScheduleController::class,'edit']);
Route::get('/schedulePost',[ScheduleController::class,'post']);


