<x-app-layout>
  <x-navigation>

  </x-navigation>

  <div>
    <div class="grid min-h-screen place-items-center">
      <div class="w-11/12 p-12 bg-white sm:w-8/12 md:w-1/2 lg:w-5/12">
        <h1 class="text-xl font-semibold text-center">Public a new post 👋</h1>
        <form method="POST" action="/newpost">
          @csrf
          <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />
          <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Titule</label>
          <input type="text" name="title" placeholder="Title" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
          <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Description</label>
          <input type="text" name="description" placeholder="Description" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />
          <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Publish later</label>
          <input class=" p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" type="checkbox" id="check" onchange="checkbox();">
          <select id="date" name="date" class="w-full border bg-white rounded px-3 py-2 outline-none text-gray-700" disabled >
            @foreach ($schedules as $value)
            <option value="{{ $value->id }}">
                @if ($value->day == '1')
                {{"Monday ".$value->hour}}
            @elseif ($value->day == '2')
            {{"Tuesday ".$value->hour}}
            @elseif ($value->day == '3')
            {{"Wednesday ".$value->hour}}
            @elseif ($value->day == '4')
            {{"Thurday ".$value->hour}}
            @elseif ($value->day == '5')
            {{"Friday ".$value->hour}}
            @elseif ($value->day == '6')
            {{"Saturday ".$value->hour}}
            @elseif ($value->day == '7')
                {{"Sunday ".$value->hour}}
            @endif
            </option>
            @endforeach
         </select>

          <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Social Network</label>




          <select name="social_media_id[]" class="w-full border bg-white rounded px-3 py-2 outline-none text-gray-700" multiple>
            @foreach ($socials as $social)
            <option value="{{ $social->id }}"> {{ $social->name }}</option>
            @endforeach
         </select>


          <button  class="w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-black shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
            Publish
          </button>

        </form>
      </div>
    </div>

  </div>

</x-app-layout>

<script>


    function checkbox(){
       document.getElementById("date").disabled = !document.getElementById("date").disabled;
    }
</script>
