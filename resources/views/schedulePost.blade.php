<x-app-layout>
    <x-navigation>

    </x-navigation>

    <div>
      <div class="grid min-h-screen place-items-center">
        <div class="w-11/12 p-12 bg-white sm:w-8/12 md:w-1/2 lg:w-5/12">
          <h1 class="text-xl font-semibold text-center">Public a new schedule 👋</h1>
          <form method="POST" action="/schedule">
            @csrf
            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" />

            <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Day</label>
            <select name="day" class="w-full border bg-white rounded px-3 py-2 outline-none text-gray-700" >
                @foreach ($days as $day => $value)
                <option value="{{ $day }}"> {{ $value }}</option>
                @endforeach
             </select>

            <label class="block mt-2 text-xs font-semibold text-gray-600 uppercase">Hour</label>
            <input type="text" name="hour" placeholder="Hour" class="block w-full p-3 mt-2 text-gray-700 bg-gray-200 appearance-none focus:outline-none focus:bg-gray-300 focus:shadow-inner" required />


            <button  class="w-full py-3 mt-6 font-medium tracking-widest text-white uppercase bg-black shadow-lg focus:outline-none hover:bg-gray-900 hover:shadow-none">
              Publish
            </button>

          </form>
        </div>
      </div>

    </div>

  </x-app-layout>

  <script>


      function checkbox(){
         document.getElementById("date").disabled = !document.getElementById("date").disabled;
      }
  </script>
