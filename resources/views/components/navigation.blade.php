<ul class="flex">

    <li class="flex-1 mr-2">
      <a class="text-center block border border-white rounded hover:border-gray-200 text-blue-500 hover:bg-gray-200 py-2 px-4" href="dashboard">Posts</a>
    </li>

    <li class="flex-1 mr-2">
      <a class="text-center block border border-white rounded hover:border-gray-200 text-blue-500 hover:bg-gray-200 py-2 px-4" href="schedule">Publication Schedule</a>
    </li>

</ul>
