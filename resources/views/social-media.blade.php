<x-app-layout>
    <x-navigation>

    </x-navigation>

    <div>
        <div class="w-2/3 mx-auto">
            <div class="bg-white shadow-md rounded my-6">
                <table class="text-left w-full border-collapse">
                    <!--Border collapse doesn't work on this site yet but it's available in newer tailwind versions -->
                    <thead>
                        <tr>
                            <th
                                class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                Social Media</th>
                            <th
                                class="py-4 px-6 bg-grey-lightest font-bold uppercase text-sm text-grey-dark border-b border-grey-light">
                                Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="hover:bg-grey-lighter">
                            <td class="py-4 px-6 border-b border-grey-light">Twitter</td>
                            <td class="py-4 px-6 border-b border-grey-light">


                                <a href="{{ url('/login/twitter') }}" class="p-2 pl-5 pr-5 bg-gray-500 text--100 text-lg rounded-lg focus:border-4 border-gray-300">Link social media</a>



                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</x-app-layout>
