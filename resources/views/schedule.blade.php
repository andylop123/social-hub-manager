<x-app-layout>
    <x-navigation>

    </x-navigation>
    <a href="schedulePost"
        class="pt-2 pb-2 px-4 py-2 rounded-md text-sm font-medium border-0 focus:outline-none focus:ring transition text-white bg-blue-500 hover:bg-blue-600 active:bg-blue-700 focus:ring-blue-300"
        type="submit">Create new publication schedule </a>
    <table class="items-center bg-transparent w-full border-collapse ">
        <thead>
            <tr>
                <th
                    class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    Day
                </th>
                <th
                    class="px-6 bg-blueGray-50 text-blueGray-500 align-middle border border-solid border-blueGray-100 py-3 text-xs uppercase border-l-0 border-r-0 whitespace-nowrap font-semibold text-left">
                    Hour
                </th>

            </tr>
        </thead>

        <tbody>
            @foreach ($schedules as $schedule)

            <tr>
                <th class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">

                        @if ($schedule->day == '1')
                            Monday
                        @elseif ($schedule->day == '2')
                           Tuesday
                        @elseif ($schedule->day == '3')
                            Wednesday
                        @elseif ($schedule->day == '4')
                            Thursday
                        @elseif ($schedule->day == '5')
                            Friday
                        @elseif ($schedule->day == '6')
                           Saturday
                        @elseif ($schedule->day == '7')
                            Sunday
                        @endif

                </th>
                <th class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                    {{$schedule->hour}}
                </th>
                <th
                    class="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs whitespace-nowrap p-4 text-left text-blueGray-700 ">
                    <a href="/showedit/{{ $schedule->id }}" class="bg-green-500 hover:bg-green-700 text-white font-bold py-1 px-2 border border-green-500 rounded">Edit</a>
                    <a href="/delete/{{ $schedule->id }}" class="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 border border-red-500 rounded">Delete</a>

                </th>
            </tr>
            @endforeach
        </tbody>

    </table>

</x-app-layout>
